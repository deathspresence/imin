from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from flask_wtf.file import FileField, FileRequired
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    clubs = SelectField('club', choices = [('AboutBlank', 'About Blank'),('Watergate', 'Watergate'),
                                          ('Tresor', 'Tresor'),('SuicideCircus','Suicide Circus'),('Sisyphos', 'Sisyphos'),
                                          ('SalonZurWildenRenate', 'Salon zur wilden Renate'),('RitterButzke', 'Ritter Butzke'),
                                          ('KaterBlau', 'Kater Blau'),('PrinceCharles','Prince Charles'),('MatrixClubBerlin', 'Matrix'),
                                          ('MusikUndFrieden', 'Musik und Frieden'),('KitKatClub', 'KitKat'),
                                          ('Berghain', 'Berghain'),('Griessmühle', 'Griessmühle'),('ClubDerVisionäre', 'Club der Visionäre'),
                                          ('BirgitUndBier', 'Birgit und Bier'),('ThePearl', 'The Pearl'),
                                          ('SpinderlUndKlatt', 'Spindler und Klatt'),('Farbfernsehr', 'Farbfernsehr')], validators=[DataRequired()])
    img = FileField('image', validators=[DataRequired()])
    submit = SubmitField('Calculate your chances!')