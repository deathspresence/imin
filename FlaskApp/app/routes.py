from flask import render_template, flash, redirect
from app import app
from app.forms import LoginForm
from werkzeug.utils import secure_filename
from app.imin import imin

@app.route('/', methods=['GET','POST'])
@app.route('/index', methods=['GET','POST'])
def index():
    form = LoginForm()
    if form.validate_on_submit():
        filename = secure_filename(form.img.data.filename)
        global img_with_box
        chance, img_with_box = imin(filename, form.clubs.data)
        flash('Your chance to get into the Club {} is {} Points from a Total of 100 '.format(
             form.clubs.data, round(chance[0]*100)))
        return redirect('/result')
    img_with_box = '/static/img/default.jpg'
    return render_template('index.html', title='IMIN', form=form, image=img_with_box)

@app.route('/result', methods=['GET','POST'])
def result():
    form = LoginForm()
    return render_template('result.html', title='IMIN', form=form, image=img_with_box)