import pandas as pd
import numpy as np

def generate_dummy():
    # creating a dummy dataset
    # creating list of clubs for column names
    col_names =['ShirtColor','AboutBlank','Watergate','Tresor','SuicideCircus','Sisyphos','SalonZurWildenRenate','RitterButzke','KaterBlau','PrinceCharles','MatrixClubBerlin','MusikUndFrieden','KitKatClub','Berghain','Griessmühle','ClubDerVisionäre','BirgitUndBier','ThePearl','SpinderlUndKlatt','Farbfernsehr']

    # array with 4 shirt colors and black seperately
    shirt_colors = ['white', 'red', 'blue', 'yellow','undefined']
    only_black = ['black']

    # generate the data for 4 colors
    # generate dummy data for 4 colors. generate random numbers in between 0.5 and 1
    data1 = np.random.triangular(0.5,0.75,1, size=(500,len(col_names)))

    # generate the dummy data for the black color
    data2 = np.random.triangular(0.7,0.85,1, size=(100,len(col_names)))

    # concatenate the generated data1 and data2
    gen_data = np.concatenate((data1,data2), axis=0)

    #create the dummy data
    dummy_data = pd.DataFrame(gen_data, columns=col_names)


    # adding the shirt color column
    # generate shirt color column
    color_data_1 = np.random.choice(shirt_colors,500)

    # generate shirt color column for 'black'
    color_data_2 = np.random.choice(only_black,100)

    # concatenate the data for the shirt color column in the dummy data
    gen_color_data = np.concatenate((color_data_1,color_data_2), axis=0)

    # add the array to the appropriate shirt color column in the dummy data
    dummy_data['ShirtColor'] = gen_color_data

    # shuffle data rows and reset index
    dummy_data = dummy_data.sample(frac=1).reset_index(drop=True)

    # save dummy_data as csv file
    pd.DataFrame.to_csv(dummy_data,'dummy_data.csv')
    return dummy_data
