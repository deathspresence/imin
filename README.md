# Project IMIN

### Table of Contents

1. [What is IMIN?](#What is IMIN?)
2. [Demo](#Demo)
3. [Setup of the python environment](#Setup of the python environment)
4. [How it works?](#How it works?)
5. [How to run the backend code only](#How to run the backend code)
6. [How to run the frontend code with backend in place](#How to run the frontend code with backend in place)
7. [Business Model](#Business Model)
8. [Possible improvements](#Possible improvements)


#### What is IMIN?

Imin is an object-identification web application based on the YOLO framework (You Only Look Once) for people who want to go clubbing.

There are cities which have a tough Club scene, where it is hard to get into.
While locals are more prepared, strangers or visitors don't know what clothes to wear.

Imin will identify clothes on any picture you want and based upon that, Imin wil tell you the chance in % how likely 
you will get into the chosen Club.

Currently this is a prototype which only identifies shirts with 5 colors(black, white, red, yellow and blue). Furthermore 
only the backend is ready. Anybody can download the code and run it on his/her computer through the CLI or terminal.

### Demo

This is a short demo of the prototype with frontend:

Start

[1]: demo_images/1.png
![alt text][1]

Choose Club

[2]: demo_images/2.png
![alt text][2]

choose your image in the right working directory (darkflow-master directory)

[3]: demo_images/3.png
![alt text][3]

Click "Calculate your chances"

[4]: demo_images/4.png
![alt text][4] 

See our chances

[5]: demo_images/5.png
![alt text][5]

See the identified shirt on screen

[6]: demo_images/6.png
![alt text][6]

Full view

[7]: demo_images/7.png
![alt text][7]

#### Setup of the python environment

To run the code you need to setup:
- Python 3.5 or Python 3.6 Anaconda
- Tensorflow (note Tensorflow does not work with python 3.7 yet)
- openCV
- YOLO darkflow (https://github.com/thtrieu/darkflow)
- Flask

You will need the following python packages:
- pandas
- numpy
- matplotlib
- sklearn
- xgboost
- cv2
- darkflow.net.build
- flask
- wtforms

Here are some of the sources I have used:
- https://www.youtube.com/watch?v=PyjBd7IDYZs
- https://www.youtube.com/watch?v=mcIKDJYeyFY
- https://github.com/jeffheaton/t81_558_deep_learning/blob/master/t81_558_class_01_1_overview.ipynb
- https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

#### How it works?

When you run the IMIN application, you have to provide two pieces of information: 1) your image 2) the club you want to attend.
There are two ML models which determine the chance of getting into the club:
1. YOLO 5-color-shirt Model
2. XGBoost Survey Model

Step 1 - YOLO 5-color-shirt Model
The YOLO model is extracting the information of what shirt the individual is wearing on the picture. This is done by
identifying either a black, white, yellow, blue or red shirt on the picture. The model was trained on 5596 images.
Around ~1500 images were downloaded from Google. To increase model robustness, every image was augmented 3x times. 
These augmented images increase the total amount of images. This was crucial to achieve better identification of shirts, 
because Google has mostly frontal shots of people. Pictures from other angles or perspectives are necessary for the model to identify. 
This was solved by image augmentation (reference: https://github.com/aleju/imgaug). 
The files in the folder "functions_for_model_creation" helped with downloading,
modifying, augmenting the images and creating xml files for the coordinates of the bounding boxes.

Step 2 - XGBoost Model
This Model receives information about the chosen club which will be attended and the extracted shirt color by the YOLO model.
With these information the model will be able to return the chance of getting in. The underlying data for the XGBoost model is
a dummy data. (Previously the underlying data was retrieved out of a survey
 which was made via Google Forms: https://forms.gle/Zaqgr4caXfXi7iJ1A . But due to low participants the data was unusable)

#### How to run the backend code only

After you have setup your python environment and installed all the relevant packages, you need to save the files of the 
YOLO model from the Imin repository into the specified folders of the darkflow folder on your local computer as follows:

save these files into the folder "cfg":
- tiny-yolo-voc-multiple-color-5c.cfg
- tiny-yolo-voc.cfg

save these files into the folder "ckpt":
- tiny-yolo-voc-multiple-color-5c-2750.data-00000-of-00001
- tiny-yolo-voc-multiple-color-5c-2750.index
- tiny-yolo-voc-multiple-color-5c-2750.meta
- tiny-yolo-voc-multiple-color-5c-2750.profile

save these files into the folder "weights":
- tiny-yolo-voc.weights

Now that the YOLO model which extracts the shirt colors is placed correctly in your folders, you can download 
the survey.csv and the in_or_out.py into your darkflow folder. 

The Next step is to run the "in_or_out.py" script. You will be asked to provide the exact image name which should be analyzed. 
Afterwards you have to choose one Club from the listed ones and the script will calculate the chances of 
getting in for the person depicted on the provided picture.

#### How to run the frontend code with backend in place

You need to follow the steps above to setup the backend. For the frontend to work you only need to download the 
content of the FlaskApp folder from the repository and save it in the darkflow folder.

To launch your local server in terminal and access the WebApp you need to type in the following commands in your terminal:

- $ FLASK_APP=imin_blog.py
- $ flask run

Voila! Now you can access the website. Do not forget to be in the right working directory. 
(If you wonder the 'About' Tab is not working yet)

### Business Model

No comprehensive business plan is described here.
This is just an idea of how a business model could look like for such an application.

A recommendation engine could be implemented to recommend what to wear to increase the chances of getting into a club.
The recommendation can forward the user to an online shop where the needed clothes can be purchased. 
On each purchase the operator of the application could make an agreed referral percentage.

Also other advertisements in form of banners, GoogleAds, etc. can be implemented.

#### Possible improvements
 
- update WebApp frontend (WebApp with Flask)
- have access to the application via Mobile(Telegram Bot, MobileApp)
- make YOLO 5-color-shirt Model more accurate and robust
- make XGBoost Survey Model more accurate and robust
- retrieve real Data
- expand YOLO model to identify other clothes than shirts
- build recommendation engine (what clothes to wear to get better chances)
- expand functionality with multiple people on the image




