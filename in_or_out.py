#setup
import cv2
from darkflow.net.build import TFNet
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder

# calling the function in_or_out when the script is launched
if __name__ == '__main__':
    print('Name the picture which has to be analyzed!')
    picture = input()

    # creating list of clubs for column names
    Clubs = ['ShirtColor','AboutBlank','Watergate','Tresor','SuicideCircus','Sisyphos',
             'SalonZurWildenRenate','RitterButzke','KaterBlau','PrinceCharles','MatrixClubBerlin',
             'MusikUndFrieden','KitKatClub','Berghain','Griessmühle','ClubDerVisionäre','BirgitUndBier',
             'ThePearl','SpinderlUndKlatt','Farbfernsehr']

    print('Name the Club you want to get in?')
    print(Clubs)
    club = input()

    # Importing the image and extracting the shirt color information

    # define the model options and run

    options = {
        'model': 'cfg/tiny-yolo-voc-multiple-color-5c.cfg',
        'load': 2750,  # specify the step or the weight file
        'threshold': 0.05
    }

    tfnet = TFNet(options)

    # read the image
    img = cv2.imread(picture, cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # use YOLO to predict the image
    result = tfnet.return_predict(img)

    # pull out some info about the shirt color from the result

    if result == []:
        label = 'undefined'
    else:
        label = result[0]['label']

    # define the color
    label_pairs = {'red_shirt': 'red', 'black_shirt': 'black', 'yellow_shirt': 'yellow', 'blue_shirt': 'blue',
                   'white_shirt': 'white', 'undefined': 'undefined'}

    chosen_color = label_pairs[label]

    # importing the dummy data
    rel_results = pd.read_csv('dummy_data/dummy_data.csv')

    # prepare the data for all the models
    models = []
    for col in rel_results.columns:
        models.append(rel_results[['ShirtColor', col]])

    # prepare data for model
    model = rel_results[['ShirtColor', club]]

    # Select subset of predictors
    cols_to_use = ['ShirtColor']
    X = rel_results[cols_to_use]

    # Select target
    y = rel_results[club]

    # Separate data into training and validation sets
    X_train, X_valid, y_train, y_valid = train_test_split(X, y)

    # Get list of categorical variables
    s = (X_train.dtypes == 'object')
    object_cols = list(s[s].index)

    # Make copy to avoid changing original data
    label_X_train = X_train.copy()
    label_X_valid = X_valid.copy()

    # Apply label encoder to each column with categorical data
    label_encoder = LabelEncoder()
    for col in object_cols:
        label_X_train[col] = label_encoder.fit_transform(X_train[col])
        label_X_valid[col] = label_encoder.transform(X_valid[col])

    my_model = XGBRegressor()
    my_model.fit(label_X_train, y_train)

    # check the label
    print('You re wearing a ...')
    print(label)

    # performing the prediction
    print('Your chance to get in is...')
    in_put = pd.DataFrame()
    in_put['ShirtColor'] = [chosen_color]
    for col in object_cols:
        in_put[col] = label_encoder.transform(in_put[col])

    chance = my_model.predict(in_put)
    print(chance)
